﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Controllers {
    public class BlogsController : Controller {
        private readonly IBlogRepository _blogsRepo;
        private readonly IConfiguration _config;

        public BlogsController(IBlogRepository blogsRepo, IConfiguration config) {
            _config = config;
            _blogsRepo = blogsRepo;
        }

        public IActionResult Index() {
            var list = _blogsRepo.GetBlogs();
            return View(list);
        }

        [HttpGet]
        public IActionResult Create() {
            return View();
        }

        //this is httppost
        [HttpPost]
        public IActionResult Create(IFormFile logo, Blog b) {
            try {
                string bucketName = _config.GetSection("AppSettings").GetSection("BucketName").Value;
                string uniqueFilename = Guid.NewGuid() + System.IO.Path.GetExtension(logo.FileName);

                //upload a file in the cloud storage
                var storage = StorageClient.Create();

                using (var myStream = logo.OpenReadStream()) {
                    storage.UploadObject(bucketName, uniqueFilename, null, myStream);
                }

                //add a reference to the file url with the instance of the blog >>> b
                b.Url = $"https://storage.googleapis.com/{bucketName}/{uniqueFilename}";


                //save everything in db
                _blogsRepo.AddBlog(b);
                TempData["message"] = $"Blog {b.Url} created successfully.";
                return RedirectToAction("Index");
            } catch (Exception ex) {
                TempData["error"] = "Blog wasn't created successfully.";
                return View(b);
            }
        }

        public IActionResult Delete(int id) {
            try {
                _blogsRepo.DeleteBlog(id);
                TempData["message"] = $"Blog deleted successfully.";
            } catch (Exception ex) {
                TempData["error"] = $"Blog wasn't deleted successfully.";
            }

            return RedirectToAction("Index");
        }
    }
}
