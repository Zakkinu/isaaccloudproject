﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Controllers {
    [Authorize(Roles = "PASSENGER")]
    public class MapController : Controller {

        private readonly IPubSubRepository _pubSubRepository;
        private readonly IConfiguration _config;

        public MapController(IPubSubRepository pubSubRepository, IConfiguration config) {
            _config = config;
            _pubSubRepository = pubSubRepository;
        }
        public IActionResult Index() {
            return View();
        }

        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Booking booking) {
            try {
                string emailReceipient = HttpContext.User.Identity.Name;
                _pubSubRepository.PublishBooking(booking, emailReceipient, booking.Type);
                TempData["message"] = $"Booking requested.";
                return View("Views/Home/index.cshtml");
            } catch (Exception e) {
                TempData["Error"] = "Booking error.";
                return View(booking);
            }
        }
    }
}
