﻿using CloudAssignment.DataAccess.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Controllers {
    [Authorize(Roles = "ADMIN")]
    public class SubbedBookingsController : Controller {

        private readonly IConfiguration _config;
        private readonly ISubbedBookingsRepository _bookingRepo;
        private readonly ILogRepository _cloudLogger;

        public SubbedBookingsController(IConfiguration config, ISubbedBookingsRepository bookingRepo, ILogRepository cloudLogger) {
            _config = config;
            _bookingRepo = bookingRepo;
            _cloudLogger = cloudLogger;
        }

        
        public IActionResult Index() {
            try {
                var list = _bookingRepo.GetBookings();
                return View(list);
            } catch(Exception e) {
                _cloudLogger.Log(e.Message, Google.Cloud.Logging.Type.LogSeverity.Error);
            }
            return View();
        }
    }
}
