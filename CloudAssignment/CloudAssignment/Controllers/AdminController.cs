﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Controllers {
    [Authorize(Roles = "ADMIN")]
    public class AdminController : Controller {
        private readonly ITypeRepository _typeRepo;

        public AdminController(ITypeRepository typeR) {
            _typeRepo = typeR;
        }
        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CarTypes Ctype) {
            _typeRepo.UpserType(Ctype);
            return View();
        }
    }
}
