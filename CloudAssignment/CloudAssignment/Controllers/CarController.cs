﻿using System;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;

namespace CloudAssignment.Controllers {

    [Authorize(Roles = "DRIVER")]
    public class CarController : Controller {
        private readonly IConfiguration _config;
        private readonly ICarRepository _carsRepo;

        public CarController(ICarRepository carsRepo, IConfiguration config) {
            _config = config;
            _carsRepo = carsRepo;
        }


        public IActionResult Index() {
            var list = _carsRepo.GetCars();
            return View(list);
        }

        [HttpGet]
        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        public IActionResult Create(IFormFile logo, DriverCar c) {
            try {
                c.CarOwner = HttpContext.User.Identity.Name;
                string bucketName = _config.GetSection("AppSettings").GetSection("BucketName").Value;
                string uniqueFilename = Guid.NewGuid() + System.IO.Path.GetExtension(logo.FileName);

                //upload a file in the cloud storage
                var storage = StorageClient.Create();

                using (var myStream = logo.OpenReadStream()) {
                    storage.UploadObject(bucketName, uniqueFilename, null, myStream);
                }

                //add a reference to the file url with the instance of the blog >>> b
                c.PhotoUrl = $"https://storage.googleapis.com/{bucketName}/{uniqueFilename}";


                //save everything in db
                _carsRepo.AddCar(c);
                TempData["message"] = $"Car {c.PhotoUrl} created successfully.";
                return RedirectToAction("Index");
            } catch (Exception ex) {
                TempData["error"] = "Car wasn't created successfully.";
                return View(c);
            }
        }

    }
}
