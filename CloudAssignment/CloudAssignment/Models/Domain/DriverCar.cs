﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
namespace CloudAssignment.Models.Domain {
    public class DriverCar {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CarId { get; set; }
        public string CarOwner { get; set; }
        public bool Airconitioned { get; set; }
        public bool FoodandDrinks { get; set; }
        public string PhotoUrl { get; set; }
        public string Type { get; set; }
        public int Capacity { get; set; }
        public string Condition { get; set; }
        public bool Wifi { get; set; }
        public string RegistrationPlate { get; set; }
    }
}
