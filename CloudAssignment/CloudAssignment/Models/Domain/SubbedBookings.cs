﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Models.Domain {
    public class SubbedBookings {
        public string DriverEmail { get; set; }
        public string PassengerEmail { get; set; }
        public string ServiceType { get; set; }
        public Guid CarId { get; set; }
    }
}
