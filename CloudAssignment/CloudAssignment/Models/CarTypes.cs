﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Models {
    public class CarTypes {
        public string ServiceName { get; set; }
        public double ServicePrice { get; set; }
    }
}
