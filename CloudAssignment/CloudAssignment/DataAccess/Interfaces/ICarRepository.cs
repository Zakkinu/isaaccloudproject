﻿using CloudAssignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface ICarRepository {
        void AddCar(DriverCar c);
        DriverCar GetCar(Guid id);

        IQueryable<DriverCar> GetCars();
        List<string> GetCarsByOwner(string owner);
        DriverCar GetCarsByOwnerAndType(string owner, string serviceType);
    }
}
