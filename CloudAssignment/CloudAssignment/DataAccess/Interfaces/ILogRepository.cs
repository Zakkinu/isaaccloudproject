﻿using Google.Cloud.Logging.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface ILogRepository {
        void Log(string message, LogSeverity severity);
    }
}
