﻿using CloudAssignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface IBlogRepository {
        void AddBlog(Blog b);
        Blog GetBlog(int id);
        IQueryable<Blog> GetBlogs();

        void DeleteBlog(int id);

        void UpdateBlog(Blog b);
    }
}
