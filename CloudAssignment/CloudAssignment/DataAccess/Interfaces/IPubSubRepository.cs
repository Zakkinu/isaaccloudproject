﻿using CloudAssignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CloudAssignment.DataAccess.Repositries.PubSubRepository;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface IPubSubRepository {
        void PublishBooking(Booking b, string email, string serviceType);

        List<Booking> PullServices(string cat);

        void AcknowledgeMessage(string ackId, string category);
    }
}
