﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Google.Cloud.Firestore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Repositries {
    public class CarUploadRepository : ICarRepository {
        FirestoreDb db;


        public CarUploadRepository(IConfiguration config) {
            var projId = config.GetSection("AppSettings").GetSection("ProjectId").Value;
            db = FirestoreDb.Create(projId); // we are creating a reference to the db;
        }

        public void AddCar(DriverCar c) {

            DocumentReference docRef = db.Collection("cars").Document();
            Dictionary<string, object> car = new Dictionary<string, object>
            {
                { "CarId", (Guid.NewGuid()).ToString() },
                { "CarOwner", c.CarOwner },
                { "PhotoUrl", c.PhotoUrl },
                { "Type", c.Type },
                { "Capacity", c.Capacity },
                { "Condition", c.Condition },
                { "RegistrationPlate", c.RegistrationPlate },
                { "Wifi", c.Wifi },
                { "Airconitioned", c.Airconitioned },
                { "FoodandDrinks", c.FoodandDrinks }
            };
            docRef.SetAsync(car).Wait();
        }

        public DriverCar GetCar(Guid id) {
            Query allCarsQuery = db.Collection("cars").WhereEqualTo("CarId", id);

            Task<QuerySnapshot> t = allCarsQuery.GetSnapshotAsync();
            t.Wait();

            QuerySnapshot allCarsQuerySnapshot = t.Result;
            DocumentSnapshot documentSnapshot = allCarsQuerySnapshot.Documents[0]; //cant have more than 1 with the same id so no need for a forloop

            Dictionary<string, object> car = documentSnapshot.ToDictionary();
            DriverCar myCar = new DriverCar();
            myCar.CarId = car.ContainsKey("CarId") ? Guid.Parse(car["CarId"].ToString()) : Guid.Parse("".ToString());
            myCar.CarOwner = car.ContainsKey("CarOwner") ? car["CarOwner"].ToString() : "";
            myCar.PhotoUrl = car.ContainsKey("PhotoUrl") ? car["PhotoUrl"].ToString() : "";
            myCar.Type = car.ContainsKey("Type") ? car["Type"].ToString() : "";
            myCar.Capacity = car.ContainsKey("Capacity") ? Convert.ToInt32(car["Capacity"].ToString()) : 0;
            myCar.Condition = car.ContainsKey("Condition") ? car["Condition"].ToString() : "";
            myCar.RegistrationPlate = car.ContainsKey("RegistrationPlate") ? car["RegistrationPlate"].ToString() : "";
            myCar.Wifi = car.ContainsKey("Wifi") ? bool.Parse(car["Wifi"].ToString()) : false;
            myCar.Airconitioned = car.ContainsKey("Airconitioned") ? bool.Parse(car["Airconitioned"].ToString()) : false;
            myCar.FoodandDrinks = car.ContainsKey("FoodandDrinks") ? bool.Parse(car["FoodandDrinks"].ToString()) : false;

            return myCar;
        }

        public List<string> GetCarsByOwner(string owner) {
            {
                Query allCarsQuery = db.Collection("cars").WhereEqualTo("CarOwner", owner);

                Task<QuerySnapshot> t = allCarsQuery.GetSnapshotAsync();
                t.Wait();

                QuerySnapshot allCarsQuerySnapshot = t.Result;

                List<string> myCars = new List<string>();

                foreach (DocumentSnapshot documentSnapshot in allCarsQuerySnapshot.Documents) {
                    Dictionary<string, object> car = documentSnapshot.ToDictionary();
                    DriverCar myCar = new DriverCar();
                    myCar.Type = car.ContainsKey("Type") ? car["Type"].ToString() : "";
                    if (!myCars.Contains(myCar.Type)) {
                        myCars.Add(myCar.Type);
                    }

                }

                return myCars;
            }
        }

        public DriverCar GetCarsByOwnerAndType(string owner, string serviceType) {
            {
                Query allCarsQuery = db.Collection("cars").WhereEqualTo("CarOwner", owner).WhereEqualTo("Type", serviceType);


                Task<QuerySnapshot> t = allCarsQuery.GetSnapshotAsync();
                t.Wait();

                QuerySnapshot allCarsQuerySnapshot = t.Result;

                //string myCarUrl = "";

                DriverCar myCar = new DriverCar();
                foreach (DocumentSnapshot documentSnapshot in allCarsQuerySnapshot.Documents) {
                    Dictionary<string, object> car = documentSnapshot.ToDictionary();

                    myCar.CarId = car.ContainsKey("CarId") ? Guid.Parse(car["CarId"].ToString()) : Guid.Parse("".ToString());
                    myCar.PhotoUrl = car.ContainsKey("PhotoUrl") ? car["PhotoUrl"].ToString() : "";

                    //myCarUrl = myCar.PhotoUrl;
                }

                return myCar;
            }
        }


        public IQueryable<DriverCar> GetCars() {

            Query allCarsQuery = db.Collection("cars");

            Task<QuerySnapshot> t = allCarsQuery.GetSnapshotAsync();
            t.Wait();

            QuerySnapshot allCarsQuerySnapshot = t.Result;

            List<DriverCar> myCars = new List<DriverCar>();

            foreach (DocumentSnapshot documentSnapshot in allCarsQuerySnapshot.Documents) {
                Dictionary<string, object> car = documentSnapshot.ToDictionary();
                DriverCar myCar = new DriverCar();

                myCar.CarId = car.ContainsKey("CarId") ? Guid.Parse(car["CarId"].ToString()) : Guid.Parse("".ToString());
                myCar.CarOwner = car.ContainsKey("CarOwner") ? car["CarOwner"].ToString() : "";
                myCar.PhotoUrl = car.ContainsKey("PhotoUrl") ? car["PhotoUrl"].ToString() : "";
                myCar.Type = car.ContainsKey("Type") ? car["Type"].ToString() : "";
                myCar.Capacity = car.ContainsKey("Capacity") ? Convert.ToInt32(car["Capacity"].ToString()) : 0;
                myCar.Condition = car.ContainsKey("Condition") ? car["Condition"].ToString() : "";
                myCar.RegistrationPlate = car.ContainsKey("RegistrationPlate") ? car["RegistrationPlate"].ToString() : "";
                myCar.Wifi = car.ContainsKey("Wifi") ? bool.Parse(car["Wifi"].ToString()) : false;
                myCar.Airconitioned = car.ContainsKey("Airconitioned") ? bool.Parse(car["Airconitioned"].ToString()) : false;
                myCar.FoodandDrinks = car.ContainsKey("FoodandDrinks") ? bool.Parse(car["FoodandDrinks"].ToString()) : false;

                myCars.Add(myCar);
            }

            return myCars.AsQueryable();

        }
    }
}
