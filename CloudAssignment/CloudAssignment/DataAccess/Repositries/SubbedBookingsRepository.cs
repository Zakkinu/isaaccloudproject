﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Google.Cloud.Firestore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Repositries {
    public class SubbedBookingsRepository : ISubbedBookingsRepository {
        //Using firestore
        FirestoreDb db;
        private readonly ILogRepository _cloudLogger;

        public SubbedBookingsRepository(IConfiguration config) {
            var projId = config.GetSection("AppSettings").GetSection("ProjectId").Value;
            db = FirestoreDb.Create(projId); // we are creating a reference to the db;
        }

        public void AddConfirmedBooking(SubbedBookings cb) {

            try {
                DocumentReference docRef = db.Collection("Bookings").Document();
                Dictionary<string, object> booking = new Dictionary<string, object>
                {
                    { "DriverEmail", cb.DriverEmail },
                    { "PassengerEmail", cb.PassengerEmail },
                    { "ServiceType", cb.ServiceType },
                    { "CarId", cb.CarId.ToString() },
                };
                docRef.SetAsync(booking).Wait();
            } catch (Exception e) {
                _cloudLogger.Log(e.Message, Google.Cloud.Logging.Type.LogSeverity.Error);
            }
        }

        public IQueryable<SubbedBookings> GetBookings() {
            Query allBookingsQuery = db.Collection("Bookings");

            Task<QuerySnapshot> t = allBookingsQuery.GetSnapshotAsync();
            t.Wait();

            QuerySnapshot allBookingsQuerySnapshot = t.Result;

            List<SubbedBookings> confirmedBookings = new List<SubbedBookings>();

            foreach (DocumentSnapshot documentSnapshot in allBookingsQuerySnapshot.Documents) {
                Dictionary<string, object> car = documentSnapshot.ToDictionary();
                SubbedBookings bookings = new SubbedBookings();

                bookings.DriverEmail = car.ContainsKey("DriverEmail") ? car["DriverEmail"].ToString() : "";
                bookings.PassengerEmail = car.ContainsKey("PassengerEmail") ? car["PassengerEmail"].ToString() : "";
                bookings.ServiceType = car.ContainsKey("ServiceType") ? car["ServiceType"].ToString() : "";
                bookings.CarId = car.ContainsKey("CarId") ? Guid.Parse(car["CarId"].ToString()) : Guid.Parse("".ToString());

                confirmedBookings.Add(bookings);
            }

            return confirmedBookings.AsQueryable();
        }
    }
}
