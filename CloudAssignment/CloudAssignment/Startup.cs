using CloudAssignment.Data;
using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.DataAccess.Repositries;
using Google.Cloud.SecretManager.V1;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment {
    public class Startup {
        public Startup(IConfiguration configuration, IWebHostEnvironment host) {
            Configuration = configuration;
            System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", host.ContentRootPath+ @"/CloudAssignment-324b3c4661c9.json");
        }

        private void CheckSameSite(HttpContext httpContext, CookieOptions options) {
            if (options.SameSite == SameSiteMode.None) {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                options.SameSite = SameSiteMode.Unspecified;
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
                options.OnAppendCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });

            string defaultConnection = GetConnectionString("DefaultConnection");

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(defaultConnection));


            //services.AddScoped<IBlogRepository, BlogsRepository>(); //when it encounters this, it is using blogsRepository (Postgres not firestore)
            services.AddScoped<IBlogRepository, BlogsFirestoreRepository>(); //if we want to use firestore and not postgres
            services.AddScoped<ICarRepository, CarUploadRepository>(); //if we want to use firestore and not postgres
            services.AddScoped<ICacheRepository, CacheRepository>();
            services.AddScoped<ITypeRepository, TypeRepository>();
            services.AddScoped<IPubSubRepository, PubSubRepository>();
            services.AddScoped<ISubbedBookingsRepository, SubbedBookingsRepository>();
            services.AddScoped<ILogRepository, LogRepository>();
            //how you can register classes + interfaces which you have created in IoC

            /*    services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<ApplicationDbContext>();*/

            services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = false)
                 .AddDefaultUI()
                 .AddEntityFrameworkStores<ApplicationDbContext>()
                 .AddDefaultTokenProviders();

            services.AddControllersWithViews();
            services.AddRazorPages();

            string clientId = GetGoogleClientIdOrSecret("ClientId");
            string secretKey = GetGoogleClientIdOrSecret("ClientSecret");

            services.AddAuthentication()
                    .AddGoogle(options => {
                        IConfigurationSection googleAuthNSection =
                            Configuration.GetSection("Authentication:Google");

                        options.ClientId = clientId;
                        options.ClientSecret = secretKey;
                    });
        }

        public string GetGoogleClientIdOrSecret(string key) {

            //for connection string
            //oAuth2.0 cliend id + secret key DONE
            //3rd party api key (mail gun) 


            SecretManagerServiceClient client = SecretManagerServiceClient.Create();

            // Build the resource name.
            SecretVersionName secretVersionName = new SecretVersionName("cloudassignment-308114", "cloudSecret", "1");

            // Call the API.
            AccessSecretVersionResponse result = client.AccessSecretVersion(secretVersionName);

            // Convert the payload to a string. Payloads are bytes by default.
            String payload = result.Payload.Data.ToStringUtf8();

            dynamic keys = JsonConvert.DeserializeObject(payload);


            JObject jObject = JObject.Parse(payload);
            JToken jKey = jObject["Authentication:Google:" + key];
            return jKey.ToString();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            } else {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
        public string GetConnectionString(string key) {

            //for connection string
            SecretManagerServiceClient client = SecretManagerServiceClient.Create();

            // Build the resource name.
            SecretVersionName secretVersionName = new SecretVersionName("cloudassignment-308114", "CloudConnections", "1");

            // Call the API.
            AccessSecretVersionResponse result = client.AccessSecretVersion(secretVersionName);

            // Convert the payload to a string. Payloads are bytes by default.
            String payload = result.Payload.Data.ToStringUtf8();

            dynamic keys = JsonConvert.DeserializeObject(payload);


            JObject jObject = JObject.Parse(payload);
            JToken jKey = jObject[key];
            return jKey.ToString();


        }
    }
}
